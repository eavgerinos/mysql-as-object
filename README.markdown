#Serial Port Communication

##Description

This code is part of an Openlab's Project (soon to be announced).

It creates a connection with a USB (Serial not HID) port.

It waits for data to be received from the USB Device [Arduino for our project] and displays it.

It can also send data to the Device.

##Credits

Author: Evaggelos Avgerinos